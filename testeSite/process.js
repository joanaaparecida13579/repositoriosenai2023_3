// Array para armazenar os itens do carrinho
var itensCarrinho = [];

// Função para adicionar um item ao carrinho
function adicionarItem() {
  // Obter informações do produto do usuário
  var produto = prompt("Digite o nome do produto:");
  var preco = parseFloat(prompt("Digite o preço do produto:"));
  var quantidade = parseInt(prompt("Digite a quantidade desejada:"));

  // Calcular o total do item
  var total = preco * quantidade;

  // Criar objeto de item
  var item = {
    produto: produto,
    preco: preco,
    quantidade: quantidade,
    total: total
  };

  // Adicionar item ao carrinho
  itensCarrinho.push(item);

  // Atualizar a exibição do carrinho
  atualizarCarrinho();
}

// Função para atualizar a exibição do carrinho
function atualizarCarrinho() {
  var carrinhoTable = document.getElementById("carrinho");

  // Limpar itens antigos
  carrinhoTable.innerHTML = "<tr><th>Produto</th><th>Preço</th><th>Quantidade</th><th>Total</th></tr>";

  // Adicionar os itens ao carrinho
  for (var i = 0; i < itensCarrinho.length; i++) {
    var item = itensCarrinho[i];
    var row = "<tr><td>" + item.produto + "</td><td>" + item.preco + "</td><td>" + item.quantidade + "</td><td>" + item.total + "</td></tr>";
    carrinhoTable.innerHTML += row;
  }
}

// Função para limpar o carrinho
function limparCarrinho() {
  // Limpar o array de itens do carrinho
  itensCarrinho = [];

  // Atualizar a exibição do carrinho
  atualizarCarrinho();
}
