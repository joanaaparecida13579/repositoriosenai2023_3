const {createApp} = Vue;

createApp({
    data(){
      return{
        logado: false,
        fazer_login: true,
        fazer_cadastro: false,
        username:"",
        password:"",
        userAdmin: false,
        error: null,
        sucesso: null,
        userAdmin: false,

        
        //Declaração dos arrays para armazenamento de usuários e senhas
        emails: ["admin@gmail.com", "joana@gmail.com", "julia@gmail.com"],
        senhas: ["1234", "1234", "1234"],
        mostrarEntrada: false,
        mostrarLista: false,

        newUsername:"",
        newEmail:"",
        confirmEmail:"",
      }  
    },//Fechamento data

    methods:{
        login(){
            this.mostrarEntrada = false;
            if(localStorage.getItem("usuarios") && localStorage.getItem(senhas)){

                this.usuarios = JSON.parse(localStorage.getItem("emails"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }
             //Variáveis para armazenamento das entradas do formulário da página de cadastro
            
            

            setTimeout(() => {
                this.mostrarEntrada = true;
                const index = this.usuarios.indexOf(this.emails);
                if(index !== -1 && this.senhas[index] === this.password){

                    localStorage.setItem("emails", this.emails);
                    localStorage.setItem("senhas", this.senhas);
    
                    this.error = null;
                    this.sucesso = "Login  efetuado com sucesso!";
                    if(this.username === "admin@gmail.com" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";
                    }
                }//Fechamento if
                else{
                    this.error = " Nome ou senha incorretos!";
                    this.sucesso = null;
                }
                this.username = "";
                this.password = "";               
            }, 1000);

           
        },//Fechamento Login2


        adicionarUsuario(){
            
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");

            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.username === "admin"){
                    if(this.newUsername !== ""){
                        if(!this.usuarios.includes(this.newUsername)){
                            if(this.newPassword && this.newPassword === this.confirmPassword){
                                this.usuarios.push(this.newUsername);
                                this.senhas.push(this.newPassword);

                                //Armazenamento do usuário e senha no LocalStorage
                                localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                                localStorage.setItem("senhas", JSON.stringify(this.senhas))

                                this.error = null;
                                this.sucesso = "Usuário cadastrado com sucesso!";
                            }
                            else{
                                this.sucesso = null;
                                this.error = "Por favor, confirme sua senha!!!";
                            }//Fechamento else
                        }//Fechamento if includes
                        else{
                            this.sucesso = null;
                            this.error = "O usuário informado já está cadastrado!";
                        }//Fechamento if !== ""
                    }                   
                    else{
                        this.error = "Por favor, digite um nome de usuário!";
                        this.sucesso = null;
                    }//Fechamento else                   
                }//Fechamento if
                else{
                    this.error = "Usuário não ADM!!!";
                    this.sucesso = null;
                }

                this.newUsername = "" ;
                this.newPassword = "";
                this.confirmPassword = "";
                
            }, 500);//Fechamento setTimeout 
            
        },//Fechamento adicionarUsuario

        verCadastrados(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//Fechamento verCadastrados

        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                //Impedir a exclusão do usuário admin
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "O usuário ADM não pode ser excluído!!!";
                }, 500);
                return;//Força a finalização do bloco / função
            }//Fechamento if usuarios
            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, );
                    this.senhas.splice(index, 1);

                    //Atualização dos vetores no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("Senhas", JSON.stringify(this.senhas));
                }//Fechamento index
            }//Fechamento do if confirm

        },//Fechamento excluirUsuarios

        fazerCadastro()
        {
            this.fazer_login = false;
            this.fazer_cadastro = true;
        },

        fazerLogin()
        {
            this.fazer_login = true;
            this.fazer_cadastro = false;
        },
    },//Fechamento methods

}).mount("#app");