const {createApp} = Vue;

createApp({
    data(){
        return{
            products:[
                {
                    id: 1,
                    name: "Cupcake",
                    // description: "Um par de tênis confortável para esportes",
                    price: 29.99,
                    image: "./imagens/cupcake.png",
                }, //Fechamento item 1
                {
                    id:2,
                    name: "banoffe",
                    // description: "Botas elegantes para qualquer ocasião",
                    price:19.99,
                    image: "./imagens/banoffe.png"
                }, //Fechamento item 2
                {
                    id:3,
                    name: "Bolo de Chocolate",
                    // description: "Sapatos clássicos para um visual sofisticado!",
                    price:49.99,
                    image: "./imagens/bolodechocolate.png",
                }, //Fechamento item 3
                {
                    id:4,
                    name: "Bolo Ganache",
                    // description: "Sandálias confortáveis para os seus pés!",
                    price:39.99,
                    image: "./imagens/bologanache.png",
                }, //Fechamento item 4
                {
                    id:5,
                    name: "Brownie",
                    // description: "Sandálias confortáveis para os seus pés!",
                    price:9.99,
                    image: "./imagens/brownie.png",
                }, //Fechamento item 5
                {
                    id:6,
                    name: "Macaron",
                    // description: "Sandálias confortáveis para os seus pés!",
                    price:12.99,
                    image: "./imagens/macaron.png",
                }, //Fechamento item 6
                {
                    id:7,
                    name: "Pão de Queijo",
                    // description: "Sandálias confortáveis para os seus pés!",
                    price:19.99,
                    image: "./imagens/paodequeijo.png",
                }, //Fechamento item 7
                {
                    id:8,
                    name: "Torta de limão",
                    // description: "Sandálias confortáveis para os seus pés!",
                    price:29.99,
                    image: "./imagens/tortadelimao.png",
                }, //Fechamento item 8
                {
                    id:9,
                    name: "Torta Holandesa",
                    // description: "Sandálias confortáveis para os seus pés!",
                    price:49.99,
                    image: "./imagens/tortaholandesa.png",
                }, //Fechamento item 9
            ], //Fechamento products
            currentProduct: {}, //Produto atual
            cart:[],

        }; //Fechamento return
    }, //Fechamento data

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();

    }, //Fechamento mounted

    //Função VUE para retornar resultados específicos de um bloco programado 
    computed:{
        cartItemCount(){
            return this.cart.length;
        },//Fechamento cartItemCount

        cartTotal(){
            return this.cart.reduce((total, product) => total + product.price, 0);
        },//Fechamento carTotal
    },//Fechamento computed

    methods:{
        updateProduct(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};

        },//Fechamento updateProduct

        addToCart(product){
            this.cart.push(product);
        },//Fechamento addToCart

        removeFromCart(product){
            const index = this.cart.indexOf(product);
            if(index != -1){
                this.cart.splice(index,1);
            }
        },//Fechamento remove


    },//Fechamento methods
}).mount("#app"); //Fechamento createApp